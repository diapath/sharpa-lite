# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


from numpy import argmax, zeros, dot


class ParsedTree:
    """Class implementing trees."""
    
    def __init__(self, feature, threshold, children_left, children_right, values):
        """Parsed tree initialization.
        
        Parameters:
        -----------
        feature: numpy array
            Array of feature used for the split in each node
        threshold: numpy array
            Array of threshold defining the split for each node
        children_left: numpy array
            Array of left nodes (feature value <= threshold)
        children_right: numpy array
            Array of right nodes (feature value > threshold)
        """
        self.feature = feature.astype(int)
        self.threshold = threshold
        self.children_left = children_left.astype(int)
        self.children_right = children_right.astype(int)
        self.values = values.astype(int)


    def _branching(self, p, node=0):
        pred = -1
        if self.children_left[node] > 0:
            if p[self.feature[node]] <= self.threshold[node]:
                # Go to left child
                pred = self._branching(p, self.children_left[node])
            else:
                # Go to right child
                pred = self._branching(p, self.children_right[node])
        else:
            pred = self.values[node]

        return pred
        

    def predict(self, X):
        """Predict class of each observation described in X.
        
        Parameters:
        -----------
        X: numpy array with shape (p, N)
            Each line of X is an observation described by N features.
        
        Returns:
        --------
        y: numpy array with shape (p,)
        """
        if len(X.shape)== 2:
            y = zeros(X.shape[0], dtype=int)
            idx = 0
            for p in X:
                y[idx] = self._branching(p)
                idx += 1
        elif len(X.shape) == 1:
            y = self._branching(X)
        else:
            raise NotImplementedError()
        return y


class ParsedMultiTree:
    """Class implementing multivariate trees."""
    
    def __init__(self, feature, threshold, children_left, children_right, values):
        """Parsed tree initialization.
        
        Parameters:
        -----------
        feature: numpy array
            Array of coefficients of features used for the split in each node
        threshold: numpy array
            Array of threshold defining the split for each node
        children_left: numpy array
            Array of left nodes (feature value <= threshold)
        children_right: numpy array
            Array of right nodes (feature value > threshold)
        values: numpy array
            Array of values
        """
        self.feature = feature.astype(float)
        self.threshold = threshold
        self.children_left = children_left.astype(int)
        self.children_right = children_right.astype(int)
        self.values = values.astype(int)


    def _branching(self, p, node=0):
        pred = -1
        if self.children_left[node] > 0:
            if dot(p, self.feature[node]) <= self.threshold[node]:
                # Go to left child
                pred = self._branching(p, self.children_left[node])
            else:
                # Go to right child
                pred = self._branching(p, self.children_right[node])
        else:
            pred = self.values[node]

        return pred


    def predict(self, X):
        """Predict class of each observation described in X.
        
        Parameters:
        -----------
        X: numpy array with shape (p, N)
            Each line of X is an observation described by N features.
        
        Returns:
        --------
        y: numpy array with shape (p,)
        """
        if len(X.shape)== 2:
            y = zeros(X.shape[0], dtype=int)
            idx = 0
            for p in X:
                y[idx] = self._branching(p)
                idx += 1
        elif len(X.shape) == 1:
            y = self._branching(X)
        else:
            raise NotImplementedError()
        return y
