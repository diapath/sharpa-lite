# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


import numpy as np
from scipy.ndimage import gaussian_filter
from skimage.color import rgb2gray
from skimage.filter import threshold_otsu, hsobel, vsobel, sobel


def grad_mag(image):
    """ Return the mean value of the gradient magnitude of an image.
    """
    imgray = rgb2gray(image)
    imblur = gaussian_filter(imgray, 0.5)
    return np.mean(sobel(imblur))


def tenengrad(image):
    """ Return the focus feature defined by Tenenbaum.
    """
    imgray = rgb2gray(image)
    gy = hsobel(imgray)
    gx = vsobel(imgray)

    Ssq = (gx*gx) + (gy*gy)
    thr = threshold_otsu(Ssq)

    out = 0
    for i in range(Ssq.shape[0]):
        for j in range(Ssq.shape[1]):
            if Ssq[i,j] > thr:
                out += Ssq[i,j]
    return out