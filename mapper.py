# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


import os

from PIL import Image
from numpy import zeros, where
from exporter import export_to_ndpa
from classifier import select_classifier
from image_pooler import ImagePooler, Mask, ThresholdMask


def map_blur(filename, category):
    """Map the blurred regions of a NDPI whole slide image.
    
    Parameters
    ----------
    filename : string
        Absolute path to the WSI file.
    category : string
        Category of the classifier. Possible values: all, he or ihc (default).
    """
    tree = select_classifier(category)
    mask = ThresholdMask(filename, 0.1, 200)
    img = ImagePooler(filename, 20, mask, nb_of_tile=mask.data.size)

    blur_map = zeros(mask.data.shape, dtype='uint8')
    slide_name = os.path.splitext(filename)[0]

    for idx, tile in img.random_next():
        if not mask.data[idx]:
            if tree.predict(tile):
                blur_map[idx] = 255
            else:
                blur_map[idx] = 128

    maskim = Image.fromarray(blur_map, mode='L')
    maskim.save(slide_name + '.png')
    map_ann = Mask(where(blur_map == 255, 1, 0).astype('uint8'), mask.mag)
    export_to_ndpa(mask.slide, map_ann)
