# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


from skimage import feature
from numpy import log, uint8, pi
from numba import autojit

__all__=['contrast_1_0',
         'contrast_1_2',
         'contrast_1_4',
         'contrast_2_0',
         'contrast_2_2',
         'contrast_2_4',
         'entropy_1_0',
         'entropy_1_2',
         'entropy_1_4',
         'entropy_2_0',
         'entropy_2_2',
         'entropy_2_4',
         ]


@autojit
def _entropy(p):
    N = p.shape[0]
    entropy = 0.0
    for i in range(0, N):
        for j in range(0, N):
            if p[i, j, 0, 0] != 0:
                entropy -= p[i, j, 0, 0] * log(p[i, j, 0, 0])
    return entropy

def _contrast(p):
    return float(feature.greycoprops(p, 'contrast')[0])


def _greycomatrix(image, step, angle):
    #greycomatrix want byte not float    
    def grey_uint8(img):
        return (0.2125 * img[:,:,0] +
                0.7154 * img[:,:,1] +
                0.0721 * img[:,:,2]).astype(uint8)

    return feature.greycomatrix(grey_uint8(image), [step], [angle], normed=True)

def entropy_1_0(image):
    return _entropy(_greycomatrix(image, 1, 0))

def entropy_1_2(image):
    return _entropy(_greycomatrix(image, 1, pi/2))

def entropy_1_4(image):
    return _entropy(_greycomatrix(image, 1, pi/4))

def entropy_2_0(image):
    return _entropy(_greycomatrix(image, 2, 0))

def entropy_2_2(image):
    return _entropy(_greycomatrix(image, 2, pi/2))

def entropy_2_4(image):
    return _entropy(_greycomatrix(image, 2, pi/4))


def contrast_1_0(image):
    return _contrast(_greycomatrix(image, 1, 0))

def contrast_1_2(image):
    return _contrast(_greycomatrix(image, 1, pi/2))

def contrast_1_4(image):
    return _contrast(_greycomatrix(image, 1, pi/4))

def contrast_2_0(image):
    return _contrast(_greycomatrix(image, 2, 0))

def contrast_2_2(image):
    return _contrast(_greycomatrix(image, 2, pi/2))

def contrast_2_4(image):
    return _contrast(_greycomatrix(image, 2, pi/4))

