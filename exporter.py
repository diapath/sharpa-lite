# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


import math
from numpy import ones, asarray
from ndpiwrapper.annotations import FreehandAnnotation, NDPAFileRenderer
from skimage import morphology as mph
from skimage.measure import find_contours

def simplify_points(pts, tolerance):
    anchor = 0
    floater = len(pts) - 1
    stack = []
    keep = set()

    stack.append((anchor, floater))
    while stack:
        anchor, floater = stack.pop()

        # initialize line segment
        if pts[floater] != pts[anchor]:
            anchorX = float(pts[floater][0] - pts[anchor][0])
            anchorY = float(pts[floater][1] - pts[anchor][1])
            seg_len = math.sqrt(anchorX ** 2 + anchorY ** 2)
            # get the unit vector
            anchorX /= seg_len
            anchorY /= seg_len
        else:
            anchorX = anchorY = seg_len = 0.0

        # inner loop:
        max_dist = 0.0
        farthest = anchor + 1
        for i in range(anchor + 1, floater):
            dist_to_seg = 0.0
            # compare to anchor
            vecX = float(pts[i][0] - pts[anchor][0])
            vecY = float(pts[i][1] - pts[anchor][1])
            seg_len = math.sqrt(vecX ** 2 + vecY ** 2)
            # dot product:
            proj = vecX * anchorX + vecY * anchorY
            if proj < 0.0:
                dist_to_seg = seg_len
            else:
            # compare to floater
                vecX = float(pts[i][0] - pts[floater][0])
                vecY = float(pts[i][1] - pts[floater][1])
                seg_len = math.sqrt(vecX ** 2 + vecY ** 2)
                # dot product:
                proj = vecX * (-anchorX) + vecY * (-anchorY)
                if proj < 0.0:
                    dist_to_seg = seg_len
                else:  # calculate perpendicular distance to line (pythagorean theorem):
                    dist_to_seg = math.sqrt(abs(seg_len ** 2 - proj ** 2))
                if max_dist < dist_to_seg:
                    max_dist = dist_to_seg
                    farthest = i

        if max_dist <= tolerance: # use line segment
            keep.add(anchor)
            keep.add(floater)
        else:
            stack.append((anchor, farthest))
            stack.append((farthest, floater))

    keep = list(keep)
    keep.sort()
    return asarray([pts[i] for i in keep], dtype=int)


def export_to_ndpa(slide, mask):
    img_close = mph.closing(mask.data, selem=ones((3, 3), mask.data.dtype))
    img_open = mph.opening(img_close, selem=ones((3, 3), dtype=img_close.dtype))
    contours = find_contours(img_open, 0.5)
    polygon_list = []
    uid = 0
    for poly in contours:
        poly[:, 0], poly[:, 1] = slide.GetCoordinatesInNm(poly[:, 1], poly[:, 0], mask.mag)
        dp_poly = simplify_points(poly.tolist(), 0)
        xc, yc = (dp_poly.max(axis=0) - dp_poly.min(axis=0)) / 2
        p = FreehandAnnotation(title=uid, lens=5, x=xc, y=yc,
                               point_list=dp_poly.tolist(), color="#ff0000")
        polygon_list.append(p)
        uid += 1
    renderer = NDPAFileRenderer(annotation_list=polygon_list)
    fd = open(slide.filename + '.ndpa', 'w')
    print slide.filename + '.ndpa'
    fd.write(renderer.render())
    fd.close()
