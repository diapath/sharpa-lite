# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


""" Image Pooler Class. """
from scipy.ndimage.interpolation import zoom
from random import shuffle
from numpy import asarray, argwhere, ones_like, where, zeros, zeros_like, unravel_index, nditer
from skimage.draw import polygon
from skimage.filter import threshold_otsu
from ndpiwrapper.pyNDPRead import  NDPImage
from itertools import product


class Mask(object):
    """An Image mask.
    """
    def __init__(self, mask, mag):
        self.data = mask
        self.mag = mag
        self.bbox = self.get_bounding_box()

    def get_bounding_box(self):
        rmin = 0
        cmin = 0
        rmax, cmax = self.data.shape
        for r in xrange(rmax):
            if self.data[r,:].any():
                rmin = r
                break

        for c in xrange(cmax):
            if self.data[:,c].any():
                cmin = c
                break

        for r in xrange(rmax-1, -1, -1):
            if self.data[r,:].any():
                rmax = r
                break

        for c in xrange(cmax-1, -1, -1):
            if self.data[:,c].any():
                cmax = c
                break

        return [(cmin, rmin), (cmax, rmax)]

    def mask_tile(self, tile_size, x0, y0, mag):
        f = self.mag / mag

        if (x0 + tile_size)*f < self.bbox[0][0]:
            if (y0 + tile_size)*f < self.bbox[0][1]:
                return zeros(tile_size, dtype=bool)

        if (x0 - tile_size)*f > self.bbox[1][0]:
            if (y0 - tile_size)*f > self.bbox[1][1]:
                return zeros(tile_size, dtype=bool)
        
        u = tile_size/2.
        v = tile_size/2.
        tm = self.data[(y0-v)*f:(y0+v)*f, (x0-u)*f:(x0+u)*f]
        mask =  zoom(tm, mag/self.mag)
        
        return mask.astype(bool)

    def isinside(self, tile_size, x0, y0, mag):
        return self.data_tile(tile_size, x0, y0, mag).all()


class ThresholdMask(Mask):
    """An Image mask.
    """
    def __init__(self, path, mag, threshold=-1):
        self.slide = NDPImage(path) #TODO: make correct check for file
        image = asarray(self.slide.GetImageMap(mag).convert('L'))

        if threshold < 0:
            threshold = threshold_otsu(image)

        mask = where(image > threshold, 1, 0)
        super(ThresholdMask, self).__init__(mask, mag)


class AnnotationMask(Mask):
    def __init__(self, path, mag, annotations):
        self.slide = NDPImage(path) #TODO: make correct check for file
        image = asarray(self.slide.GetImageMap(mag).convert('L'))
        mask = ones_like(image, dtype=int)

        for ann in annotations:
            x, y = ann.point_list[:,0], ann.point_list[:,1]
            xpx, ypx = self.slide.GetCoordinatesInPixels(x, y, mag)
            rr, cc = polygon(ypx, xpx, mask.shape)
            try:
                mask[rr, cc] = 0
            except:
                pass

        super(AnnotationMask, self).__init__(mask, mag)


class ImagePooler(object):
    """Yield image tiles on request.
    """
    
    def __init__(self, path, mag, mask=None, tile_size=200, pooling='sequential', nb_of_tile=None):
        """Yield image tiles within mask on request.

        Parameters
        ----------

        path: String
        NDPI file path
        
        mag: float
        The magnification at which the tiles must be obtained
        
        mask: Mask instance
        Mask representing the regions of interest of the slide. 
        `mask.data.shape` should be equal to the size of the image at the given magnification.
        """
        if pooling != 'sequential': #TODO: implement for random pooling
            raise NameError('{0} is not a correct pooling type'.format(pooling))
        self.slide = NDPImage(path) #TODO: make correct check for file
        self.mag = float(mag)
        
        if mask is None:
            mask_mag = self.mag / tile_size
            self.mask = Mask(zeros_like(self.slide.GetImageMap(mask_mag).convert('L')), mask_mag)
        else:
            self.mask = mask

        self.tile_size = tile_size
        self.pooling = pooling

        if nb_of_tile is None:
            self.iter = nditer(self.mask.data, flags=['multi_index'])

        else:
            self.nb_of_tile = nb_of_tile
            chosen_tiles = []
            it = nditer(self.mask.data, flags=['multi_index'])
            for i in it:
                if not i:
                    chosen_tiles.append(it.multi_index)
            shuffle(chosen_tiles)
            self.iter = chosen_tiles

    def __iter__(self):
        return self

    def next(self):
        if not self.iter.finished:
            idx = self.iter.multi_index
            self.iter.iternext()
            tile = self.get_tile_excl(idx[1], idx[0])
            if tile is not None:
                return idx, tile
            else:
                pass
        else:
            self.iter.reset()
            raise StopIteration

    def random_next(self):
        c = 0
        for idx in self.iter:
            if c < self.nb_of_tile:
                tile = self.get_tile_excl(idx[1], idx[0])
                if tile is not None:
                    yield idx, tile
                    c += 1
                else:
                    pass


    def get_tile(self, x0, y0):
        """Return the tile of center (x0, y0)
        """
        x0 = x0 * (self.mag/self.mask.mag)
        y0 = y0 * (self.mag/self.mask.mag)

        tile = asarray(self.slide.GetImagePx2D(self.tile_size, self.tile_size, x0, y0, self.mag))
        
        if self.mask is None:
            return tile
        else:
            mask = self.mask.mask_tile(self.tile_size, x0, y0, self.mag)
            masked_tile = tile.copy()
             
            masked_tile[:,:,0][mask] = 0
            masked_tile[:,:,1][mask] = 0
            masked_tile[:,:,2][mask] = 0
             
            return masked_tile


    def get_tile_excl(self, x0, y0):
        """Return the tile of center (x0, y0)
        if it lies completely inside the mask
        """
        x0 = x0*(self.mag/self.mask.mag)
        y0 = y0*(self.mag/self.mask.mag)

        tile = asarray(self.slide.GetImagePx2D(self.tile_size, self.tile_size, x0, y0, self.mag))
        
        if self.mask is None:
            return tile
        else:
            mask = self.mask.mask_tile(self.tile_size, x0, y0, self.mag)
            if mask.any():
                return None
            else:
                return tile
