# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


import argparse


def create_parser():
    parser = argparse.ArgumentParser(description="Produce a map of the blurred\
                                     regions for each NDPI file in the\
                                     specified folder.")

    parser.add_argument('folder', help='Folder containing the .NDPI files.')
    
    parser.add_argument('--category', '-c', 
                        choices=['all', 'ihc', 'he'], default='ihc',
                        help='Classifier category. Defaults to ihc.')

    parser.add_argument('--dry-run', '-n', action='store_true', 
                        help='Output the tissue segmentation map only')

    return parser