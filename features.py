# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


import numpy as np

from numba import int32, float64, jit
from scipy.ndimage import gaussian_filter

from grad_features import grad_mag, tenengrad
from haralick_features import contrast_1_4, entropy_1_0

grad_mag = jit(float64(int32[:,:,:]))(grad_mag)
tenengrad = jit(float64(int32[:,:,:]))(tenengrad)


def _nb_noise(im):
    """ Compute the noise image given the feature image
    """
    height, width, depth = im.shape
    noise = np.zeros(depth, dtype=im.dtype)
    for r in range(1, height-1):
        for c in range(1, width-1):
            for d in range(depth):
                value = 256
                for rr in range(r-1, r+2):
                    for cc in range(c-1, c+2):
                        if (rr != r) or (cc != c): 
                            cur = abs(im[rr, cc, d] - im[r, c, d])
                            if (cur < value):
                                value = cur
                noise[d] += (value * value)
    out = float64(noise[0] + noise[1] + noise[2]) / (height * width * depth)
    return out

noise = jit(float64(int32[:,:,:]))(_nb_noise)


def _nb_blur_feats(im):
    height, width, depth = im.shape
    N = height * width * depth

    result = np.zeros(2, dtype=np.float64)

    for r in range(height):
        for c in range(width):
            for d in range(depth):
                v = im[r, c, d]
                result[0] += v
                result[1] += v*v
    
    result = result / N
    result[1] = np.sqrt(result[1] - (result[0] * result[0]))
    return result


blur_feats = jit(float64[:](int32[:,:,:]))(_nb_blur_feats)
