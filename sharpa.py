# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, time
from arg_parser import create_parser
from mapper import map_blur


def populate_file_list(folder):
    bn = lambda fn: os.path.splitext(fn)[0]
    maps = set([bn(f) for f in os.listdir(folder) if f.endswith('.png')])
    ndpis = set([bn(f) for f in os.listdir(folder) if f.endswith('.ndpi')])
    return ndpis.difference(maps)


if __name__ == '__main__':
    parser = create_parser()
    args = parser.parse_args()
    
    files = populate_file_list(args.folder)

    while len(files):
        filename = args.folder + '/' + files.pop() + '.ndpi'
        map_blur(filename, args.category)
