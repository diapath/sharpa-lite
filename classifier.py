# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


from numpy import zeros, int32, asarray
from scipy.ndimage import gaussian_filter
from trees import ParsedTree
from features import grad_mag, tenengrad, noise, contrast_1_4, entropy_1_0
import pandas as pd


KCLASSES = {'C':0, 'B':1}

def load_StatisticaTree(treefile, known_features, known_classes):
    tree = pd.read_csv(treefile)
    nnodes = tree.shape[0]

    feature = zeros(nnodes)
    threshold = zeros(nnodes)
    values = zeros(nnodes)
    children_left = zeros(nnodes)
    children_right = zeros(nnodes)

    for idx in range(nnodes):
        v = tree.variable[idx]
        if isinstance(v, str):
            feature[idx] = known_features[v]
            threshold[idx] = tree.threshold[idx]
            values[idx] = -1
            children_left[idx] = tree.left_child[idx] - 1
            children_right[idx] = tree.right_child[idx] - 1
        else:
            values[idx] = known_classes[tree.pred[idx]]

    return ParsedTree(feature, threshold, children_left, children_right, values)

def select_classifier(category):
    if category == 'ihc':
        return IHCClassifierTree()
    elif category == 'he':
        return HEClassifierTree()
    elif category == 'all':
        return ALLClassifierTree()


class ClassifierTree(object):
    def __init__(self, category):
        self.category = category
        self.model = 'models/{}-tree.txt'.format(category)
        self.tree = load_StatisticaTree(self.model, self.KFEATURE, KCLASSES)

    def predict(self, x):
        features = self.get_features(x)
        return self.tree.predict(features)


class ALLClassifierTree(ClassifierTree):
    def __init__(self):
        self.KFEATURE = {'contrast_1_4': 0, 
                         'grad_mag': 1,
                         'tenengrad': 2,
                         'noise': 3,}

        super(ALLClassifierTree, self).__init__('all')

    def get_features(self, x):
        out = zeros(len(self.KFEATURE))
        for k, v in self.KFEATURE.items():
            if k == 'noise':
                imagei = x.astype(int32)
                imaged = asarray(abs(imagei - gaussian_filter(imagei,2)))
                out[v] = noise(imaged)
            else:
                out[v] = eval(k + '(x)')
        return out



class IHCClassifierTree(ClassifierTree):
    def __init__(self):
        self.KFEATURE = {'contrast_1_4': 0, 
                         'grad_mag': 1,
                         'tenengrad': 2,}

        super(IHCClassifierTree, self).__init__('ihc')

    def get_features(self, x):
        out = zeros(len(self.KFEATURE))
        for k, v in self.KFEATURE.items():
            out[v] = eval(k + '(x)')
        return out


class HEClassifierTree(ClassifierTree):
    def __init__(self):
        self.KFEATURE = {'contrast_1_4': 0, 
                         'grad_mag': 1,
                         'tenengrad': 2,
                         'noise': 3,
                         'entropy_1_0': 4}

        super(HEClassifierTree, self).__init__('he')

    def get_features(self, x):
        out = zeros(len(self.KFEATURE))
        for k, v in self.KFEATURE.items():
            if k == 'noise':
                imagei = x.astype(int32)
                imaged = asarray(abs(imagei - gaussian_filter(imagei,2)))
                out[v] = noise(imaged)
            else:
                out[v] = eval(k + '(x)')
        return out
