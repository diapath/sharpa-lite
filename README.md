# Sharpa
Copyright (C) 2012-2013, Université Libre de Bruxelles.

Bring your whole slide images up to their sharpest level!

Sharpa is a sharpness assessement tool for NDPI whole slide images, also called virtual slides (VSs).

Given a folder containing the ndpi files to analyze, it creates:

- A png file showing:
	- the background image in **black**
	- correctly focused tissue regions in **grey**
	- incorrectly focussed tissue regions (blurred regions) in **white**

- A ndpa file outlining the blurred regions while viewing the VS with the proprietary NDPI viewer from Hamamatsu (NDP.view).


# Dependencies

Sharpa has been designed and tested for **Python 2.7**. To run, Sharpa needs the following libraries installed:

- numpy >= 1.7.1
- scipy >= 0.12.0
- numba >= 0.9.0
- scikit-image (skimage) >= 0.8.2
- pillow >= 2.1.0
- pandas >= 0.10.0


# Installation and usage

If you have Git, The easiest is to clone the repository:

	$ git clone git@bitbucket.org:diapath/sharpa-lite.git

If you don't, then download and extract [the archive](https://bitbucket.org/diapath/sharpa-lite/downloads#tag-downloads).

You can use sharpa by calling it from the command line:

	$ python sharpa.py /path/to/my/ndpi-files/

You can specify the technique used for colouring the slides with the *category* option:

	$ python sharpa.py --category he /path/to/my/he-ndpi-files/

or shortly:

	$ python sharpa.py -c he /path/to/my/he-ndpi-files/


# Bugs and new features

If you find a bug or need the addition of a new feature, feel free to fill an issue in [our issue tracker](https://bitbucket.org/diapath/sharpa-lite/issues?status=new&status=open)!


# License

Please read LICENSE.txt in this directory.
