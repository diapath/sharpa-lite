import unittest
from lxml import etree
from cStringIO import StringIO

from annotations import *
from numpy import array

class TestAnnotations(unittest.TestCase):
    """ Testing Annotation Parser.
    """
    
    def setUp(self):
        """ Set up the fixtures.
        """
	self.circle_file = './test/circles.ndpa'
        self.circles = [CircleAnnotation(title='Circle1', x=11000, y=12000, z=13000, lens=5.0, 
                                         atype='circle', color='#ff0000',
                                         cx=10, cy=12, radius=20),
                        CircleAnnotation(title='Circle2', x=11001, y=12001, z=13001, lens=10.0, 
                                         atype='circle', color='#00ff00',
                                         cx=20, cy=22, radius=10)]

	self.rectangle_file = './test/rectangles.ndpa'
        self.rectangles = [QuadAnnotation(title='Rect1', x=3764902, y=-2619166, z=0, lens=0.328307, 
                                          atype='quad', color='#00ff00',
                                          center=array([-6885779, -8523902]),
                                          point_list=array([[-7492813, -9078338], [-6278746, -9078338], 
                                                            [-6278746, -7969467], [-7492813, -7969467]])
                                          ),
                           QuadAnnotation(title='Rect2', x=3764902, y=-2619166, z=0, lens=0.328307, 
                                          atype='quad', color='#00ff00',
                                          center=array([-5313009, -8579345]),
                                          point_list=array([[-5920043, -9133781], [-4705976, -9133781], 
                                                            [-4705976, -8024910], [-5920043, -8024910]])
                                          )]

	self.freehand_file = './test/freehands.ndpa'
        self.polygons = [FreehandAnnotation(title='Poly1', x=3764902, y=-2619166, z=0, lens=0.328307, 
                                          atype='freehand', color='#00ff00',
                                          point_list=array([[7492813, 9078338], [6278746, 9078338], 
							    [6278746, 7969467], [7492813, 7969467],
                                                            [-7492813, -9078338], [-6278746, -9078338],
                                                            [-6278746, -7969467], [-7492813, -7969467]])
                                          ),
                           FreehandAnnotation(title='Poly2', x=3764902, y=-2619166, z=0, lens=0.328307, 
                                          atype='freehand', color='#00ff00',
                                          point_list=array([[-5920043, -9133781], [-4705976, -9133781], 
                                                            [-4705976, -8024910], [-5920043, -8024910],
                                                            [5920043, 9133781], [4705976, 9133781], 
							    [4705976, 8024910], [5920043, 8024910]])
                                          )]

        self.mixed_file = './test/mixed.ndpa'
        self.mixed = [CircleAnnotation(title='Circle1', x=11000, y=12000, z=13000, lens=5.0, 
                                         atype='circle', color='#ff0000',
                                         cx=10, cy=12, radius=20),
                      QuadAnnotation(title='Rect1', x=3764902, y=-2619166, z=0, lens=0.328307, 
                                          atype='quad', color='#00ff00',
                                          center=array([-6885779, -8523902]),
                                          point_list=array([[-7492813, -9078338], [-6278746, -9078338], 
                                                            [-6278746, -7969467], [-7492813, -7969467]])
                                          ),
                      FreehandAnnotation(title='Poly1', x=3764902, y=-2619166, z=0, lens=0.328307, 
                                          atype='freehand', color='#00ff00',
                                          point_list=array([[7492813, 9078338], [6278746, 9078338], 
							    [6278746, 7969467], [7492813, 7969467],
                                                            [-7492813, -9078338], [-6278746, -9078338],
                                                            [-6278746, -7969467], [-7492813, -7969467]])
                                          )]


    def test_circles(self):
        """ testing parsing of circle annotations.
        """
        circles_parsed = parser.get_annotation_list(self.circle_file)
        self.assertListEqual(circles_parsed, self.circles)


    def test_rectangles(self):
        """ testing parsing of rectangle annotations.
        """
        rects_parsed = parser.get_annotation_list(self.rectangle_file)
        self.assertListEqual(rects_parsed, self.rectangles)


    def test_polygons(self):
        """ testing parsing of polygon annotations.
        """
        poly_parsed = parser.get_annotation_list(self.freehand_file)
        self.assertListEqual(poly_parsed, self.polygons)

    def test_mixed(self):
        """ testing parsing of mixed annotations.
        """
        mixed_parsed = parser.get_annotation_list(self.mixed_file)
        self.assertListEqual(mixed_parsed, self.mixed)

    def test_circles_rendering(self):
        """ testing rendering of circle annotations.
        """
        r = renderer.NDPAFileRenderer(annotation_list=self.circles)
        circles_rendered = parser.get_annotation_list(StringIO(r.render()))
        self.assertListEqual(circles_rendered, self.circles)

    def test_polygons_rendering(self):
        """ testing parsing of polygon annotations.
        """
        r = renderer.NDPAFileRenderer(annotation_list=self.polygons)
        polys_rendered = parser.get_annotation_list(StringIO(r.render()))
        self.assertListEqual(polys_rendered, self.polygons)

    def test_rectangles_rendering(self):
        """ testing parsing of rectangle annotations.
        """
        r = renderer.NDPAFileRenderer(annotation_list=self.rectangles)
        rects_rendered = parser.get_annotation_list(StringIO(r.render()))
        self.assertListEqual(rects_rendered, self.rectangles)

    def test_mixed_rendering(self):
        """ testing parsing of mixed annotations.
        """
        r = renderer.NDPAFileRenderer(annotation_list=self.mixed)
        mixed_rendered = parser.get_annotation_list(StringIO(r.render()))
        self.assertListEqual(mixed_rendered, self.mixed)
