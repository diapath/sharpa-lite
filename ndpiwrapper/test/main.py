#! /usr/bin python
# -*- coding: utf-8 -*-

"""Testing elastix to register NDPI images.
"""
from loader import *

def convert_to_bmp(f, mag):
    slide = NDPImage(f)
    phy_w, phy_h, phy_x, phy_y = slide.GetMapInfo()
    width = 4*(slide.GetImageWidthPx(mag)/4)
    height = 4*(slide.GetImageHeightPx(mag)/4)

    image = slide.GetImageData(width, height, phy_x, phy_y, 0, mag)
    base_f = os.path.splitext(f)[0]

    image.convert("L").save("{0}.bmp".format(base_f))

if __name__ == "__main__":
    file =  sys.argv[1]
    mag = float(sys.argv[2])
    convert_to_bmp(file, mag)

