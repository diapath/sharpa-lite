#! /bin/usr/env python
# -*- coding: utf-8 -*-

"""Load ressources from ndpiwrapper to run the tests."""

import os, sys

def upper_levels(path, levels):
    p = path
    for i in xrange(levels):
        p = os.path.split(p)[0]
    return p

parent_dir = upper_levels(__file__,3)
sys.path.append(parent_dir)
from ndpiwrapper import NDPImage
