#! /bin/usr/env python
# -*- coding: utf-8 -*-

"""Annotation parser module."""

__author__ = "Xavier Moles Lopez <x.moleslo@gmail.com>"
__date__ = "24-nov.-2010"

from xml.etree import ElementTree as etree
from . import CircleAnnotation, FreehandAnnotation, QuadAnnotation, AnnotationError


def annotation_builder(viewstate):
    annotation_class = {'freehand': FreehandAnnotation, 'circle': CircleAnnotation, 'quad':QuadAnnotation}
    
    try:
        return annotation_class['quad'].from_viewstate(viewstate)
    except AnnotationError:
        typeid = viewstate.find('annotation').get('type')
        try:
            return annotation_class[typeid].from_viewstate(viewstate)
        except KeyError:
            raise TypeError("Unknown annotation type : {}".format(typeid))


def get_annotation_list(filename):
    """Return an lxml etree. The root of it is on the ndpviewstate."""
    xmltree = etree.parse(filename)
    root = xmltree.getroot()
    viewstate_list = root.findall('ndpviewstate')

    annotation_list = []

    for viewstate in viewstate_list:
        try:
            annotation_list.append(annotation_builder(viewstate))
        except TypeError as e:
            print e
    return annotation_list
