#! /bin/usr/env python
# -*- coding: utf-8 -*-

"""NDPA annotation module."""

__author__ = "Xavier Moles Lopez <x.moleslo@gmail.com>"
__date__ = "23-nov.-2010"

__all__ = ['parser', 'renderer', 'CircleAnnotation', 'FreehandAnnotation', 'QuadAnnotation', 'NDPAFileRenderer']
import os
from numpy import empty, int32
from jinja2 import Environment, FileSystemLoader
from enthought.traits.api import HasTraits, String, Array, Int, Float
from renderer import NDPAFileRenderer

class AnnotationError(Exception):
    def __init__(self, value):
        self.value = value


class Annotation(HasTraits):
    title = String()
    x = Int()
    y = Int()
    z = Int(0)
    lens = Float()

    atype = String()
    color = String()
    displayname = String()

    @classmethod
    def from_viewstate(cls, viewstate):
        inst = cls()
        inst.title = viewstate.find('title').text
        inst.x = int(viewstate.find('x').text)
        inst.y = int(viewstate.find('y').text)
        inst.z = int(viewstate.find('z').text)
        inst.lens = float(viewstate.find('lens').text)

        annotation = viewstate.find("annotation")
        inst.atype = annotation.get("type")
        inst.color = annotation.get("color")
        inst.displayname = annotation.get("displayname")
        return inst

    def render(self):
        env = Environment(loader=FileSystemLoader(os.path.dirname(os.path.abspath( __file__ ))+ '/templates'))
        template = env.get_template(self.atype+'.xml')
        return template.render(item=self)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return other.__dict__ == self.__dict__
        else:
            return False


class FreehandAnnotation(Annotation):
    point_list = Array(dtype=int32, shape=(None,2))
    center = Array(dtype=int32, shape=(2,))

    def __init__(self, **kwargs):
        super(FreehandAnnotation, self).__init__(**kwargs)
        self.displayname = 'Freehand Annotation'
        self.atype = 'freehand'

    
    @classmethod
    def from_viewstate(cls, viewstate):
        inst = super(FreehandAnnotation, cls()).from_viewstate(viewstate)
        if viewstate.find("annotation").get("type") != 'freehand':
            raise AnnotationError("Annotation is not Freehand.")

        annotation = viewstate.find("annotation")
        point_list = annotation.findall('pointlist/point')
        inst.point_list = empty(shape=(len(point_list), 2))
        for i,point in enumerate(point_list):
            inst.point_list[i,0] = int(point.find('x').text)
            inst.point_list[i,1] = int(point.find('y').text)
        xmin, ymin = inst.point_list.min(axis=0)
        xmax, ymax = inst.point_list.max(axis=0)
        inst.center = [int(xmin + (xmax - xmin) / 2.), int(ymin + (ymax - ymin) / 2.)]
        return inst


    def __eq__(self, other):
        if isinstance(other, self.__class__):
            eq = True
            for k in other.__dict__:
                if k == 'point_list':
                    eq &= (self.__dict__[k] == other.__dict__[k]).all()
                else:
                    eq &= self.__dict__[k] == other.__dict__[k]
            return eq
        else:
            return False


class CircleAnnotation(Annotation):
    """Circle annotation.
    """
    cx = Int()
    cy = Int()
    radius = Int()

    def __init__(self, **kwargs):
        super(CircleAnnotation, self).__init__(**kwargs)
        self.displayname = 'Circular Annotation'
        self.atype = 'circle'

    @classmethod
    def from_viewstate(cls, viewstate):
        inst = super(CircleAnnotation, cls()).from_viewstate(viewstate)
        annotation = viewstate.find("annotation")
        inst.cx = int(annotation.find('x').text)
        inst.cy = int(annotation.find('y').text)
        inst.radius = int(annotation.find('radius').text)
        return inst


class QuadAnnotation(FreehandAnnotation):
    """An annotation corresponding to a quadrilateral.
    """
    def __init__(self, **kwargs):
        super(QuadAnnotation, self).__init__(**kwargs)
        self.displayname = 'Rectangular Annotation'
        self.atype = 'quad'

    @classmethod
    def from_viewstate(cls, viewstate):
        inst = super(QuadAnnotation, cls()).from_viewstate(viewstate)
        inst.atype = 'quad'
        if inst.point_list.shape[0] != 4:
            raise AnnotationError('Annotation should be a quadrilateral.')
        inst.center = inst.point_list.mean(axis=0).astype(int)
        return inst


    def __eq__(self, other):
        if isinstance(other, self.__class__):
            eq = True
            for k in other.__dict__:
                if (k == 'point_list') | (k=='center') :
                    eq &= (self.__dict__[k] == other.__dict__[k]).all()
                else:
                    eq &= self.__dict__[k] == other.__dict__[k]
            return eq
        else:
            return False
