#! /bin/usr/env python
# -*- coding: utf-8 -*-

"""Render annotations to ndpa file."""

__author__ = "Xavier Moles Lopez <x.moleslo@gmail.com>"

import os
from jinja2 import Environment, FileSystemLoader
from traits.api import HasTraits, String, Int, Float, List


class NDPAFileRenderer(HasTraits):
    """Renderer for NDPA files."""
    annotation_list = List()

    def render(self):
        env = Environment(loader=FileSystemLoader(os.path.dirname(os.path.abspath( __file__ ))+ '/templates'))
        template = env.get_template('base.xml')
        return template.render(annotation_list=self.annotation_list)
