#! /bin/usr/env python
# -*- coding: utf-8 -*-

"""NDPI image reader."""

import os

__wd__ = os.path.abspath(os.curdir)

# Because of the NDPRead.dll dependencies, we need to change the working
# directory... I still don't get why.
p = os.path.abspath(os.path.split(__file__)[0])
os.chdir(p)
from pyNDPRead import NDPImage
os.chdir(__wd__)

__all__ = ['NDPImage', 'annotations', 'image_pooler']
