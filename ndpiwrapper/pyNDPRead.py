# Copyright (C) 2012-2013, Université Libre de Bruxelles.
# This file is part of Sharpa.
#
# Sharpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# Sharpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sharpa.  If not, see <http://www.gnu.org/licenses/>.


#! /bin/usr/env python
# -*- coding: utf-8 -*-

"""Thin Wrapper around the NDPRead library using ctypes."""
import os
from PIL import Image
from numpy import asarray
from ctypes import *

# TODO: Harmonize naming convention.

try:
    # Load x64 version by default
    hama = cdll.LoadLibrary("NDPRead-x64.dll")
except WindowsError as we:
    # Load x86 version
    print "Working with 32 bit version of NDPRead."
    hama = cdll.LoadLibrary("NDPRead.dll")
except Exception as e:
    raise e

hama.GetSourceLens.restype = c_float
hama.GetImageWidth.restype = c_long
hama.GetImageHeight.restype = c_long
hama.GetImageData.restype = c_long
hama.CleanUp.restype = c_long

class FileNotFoundError(Exception):
    """Error raised when providing a file that does not exists.
    """

    def __init__(self, missing_file_name):
        """Common interface for missing file.

        Arguments:
        - missing_file_name : name of the missing file

        """
        super(FileNotFoundError, self).__init__(missing_file_name)

        self.missing_file_name = missing_file_name

    def __str__(self):
        """Construct the error string."""
        return u'File {0} does not exists!'.format(self.missing_file_name)

class CleanUpError(Exception):
    """Error raised for clean up errors."""

    def __init__(self, cu):
        super(CleanUpError, self).__init__(cu)
        self.clean_up_code = cu

    def __str__(self):
        return u'Could not make clean up! Code returned: {0}'.format(self.clean_up_code)

class NDPImage:

    def __init__(self, filename):
        """Argument:
        filename -- string with absolute file name.
        """
        if os.path.exists(filename):
            self.filename = filename
            im = Image.open(filename)
            x = im.tag.get(282)[0][0]
            y = im.tag.get(283)[0][0]
            self.source_lens = hama.GetSourceLens(self.filename)
            self.CONV_FACT = ((1./x+1./y)/2)*self.source_lens*10**7
            self.physical_width = hama.GetImageWidth(self.filename)
            self.physical_height = hama.GetImageHeight(self.filename)
            self.find_stride = lambda buff_size, width, height: 0 if buff_size==3*width*height else buff_size/height
        else:
            raise FileNotFoundError(filename)

    def GetSourceLens(self):
        return self.source_lens

    def GetImageWidthPx(self, magnification):
        return int(1.0*magnification*self.physical_width/self.CONV_FACT)+1    

    def GetImageHeightPx(self, magnification):
        return int(1.0*magnification*self.physical_height/self.CONV_FACT)+1

    def GetSlideImageInfo(self):
        o_nPhysicalX = c_long()
        o_nPhysicalY = c_long()
        o_nPhysicalWidth = c_long()
        o_nPhysicalHeight = c_long()
        i_pBuffer = c_void_p()
        io_nBufferSize = c_long()
        o_nPixelWidth = c_long()
        o_nPixelHeight = c_long()

        hama.GetSlideImage(self.filename,
                           byref(o_nPhysicalX), byref(o_nPhysicalY),
                           byref(o_nPhysicalWidth), byref(o_nPhysicalHeight),
                           i_pBuffer, byref(io_nBufferSize),
                           byref(o_nPixelWidth), byref(o_nPixelHeight))

        return [o_nPhysicalWidth.value, o_nPhysicalHeight.value, o_nPhysicalX.value, o_nPhysicalY.value]

    def GetMapInfo(self):
        o_nPhysicalX = c_long()
        o_nPhysicalY = c_long()
        o_nPhysicalWidth = c_long()
        o_nPhysicalHeight = c_long()
        i_pBuffer = c_void_p()
        io_nBufferSize = c_long()
        o_nPixelWidth = c_long()
        o_nPixelHeight = c_long()
        hama.SetCameraResolution(100,100)

        hama.GetMap(self.filename,
                    byref(o_nPhysicalX), byref(o_nPhysicalY),
                    byref(o_nPhysicalWidth), byref(o_nPhysicalHeight),
                    i_pBuffer, byref(io_nBufferSize),
                    byref(o_nPixelWidth), byref(o_nPixelHeight))

        return [o_nPhysicalWidth.value, o_nPhysicalHeight.value, o_nPhysicalX.value, o_nPhysicalY.value]

    
    def GetMap(self, i_nWidth, i_nHeight):
        cu = hama.CleanUp()
        if cu:
            o_nPhysicalX = c_long()
            o_nPhysicalY = c_long()
            o_nPhysicalWidth = c_long()
            o_nPhysicalHeight = c_long()
            i_pBuffer = c_void_p()
            io_nBufferSize = c_long()
            o_nPixelWidth = c_long()
            o_nPixelHeight = c_long()
            hama.SetCameraResolution(i_nWidth,i_nHeight)

            hama.GetMap(self.filename,
                        byref(o_nPhysicalX), byref(o_nPhysicalY),
                        byref(o_nPhysicalWidth), byref(o_nPhysicalHeight),
                        i_pBuffer, byref(io_nBufferSize),
                        byref(o_nPixelWidth), byref(o_nPixelHeight))
            
            i_pBuffer = (c_byte * io_nBufferSize.value)()

            hama.GetMap(self.filename,
                        byref(o_nPhysicalX), byref(o_nPhysicalY),
                        byref(o_nPhysicalWidth), byref(o_nPhysicalHeight),
                        i_pBuffer, byref(io_nBufferSize),
                        byref(o_nPixelWidth), byref(o_nPixelHeight))

            stride = self.find_stride(io_nBufferSize.value, o_nPixelWidth.value, o_nPixelHeight.value)            
            return Image.frombuffer("RGB",(o_nPixelWidth.value,o_nPixelHeight.value),i_pBuffer,"raw","BGR",stride,-1)
        else:
            raise CleanUpError(cu)


    def GetImageData(self, frame_width, frame_height,x_center,y_center, z_plan, magnification):
        """Return the image given the positions in nanometers and extend in pixels.

        Arguments:

            -frame_width: long with the width in pixel.
            -frame_height: long with the height in pixel.
            -x_center: long with the physical X pos of the desired image in nm.
            -y_center: long with the physical Y pos of the desired image in nm.
            -z_plan: long with the physical Z (focal) pos of the desired image in nm.
            -magnification: long with the objective magnification.

        Output:
            
            PIL Image object containing cropped region.
        """
        i_pBuffer = c_void_p()
        i_nPhysicalXPos = int(x_center)
        i_nPhysicalYPos = int(y_center)
        i_nPhysicalZPos = int(z_plan)
        i_fMag = c_float(magnification)
        o_nPhysicalWidth = c_long()
        o_nPhysicalHeight = c_long()
        io_nBufferSize = c_long()
        hama.SetCameraResolution(int(frame_width),int(frame_height))

        hama.GetImageData(self.filename,
                          i_nPhysicalXPos, i_nPhysicalYPos, i_nPhysicalZPos,
                          i_fMag, byref(o_nPhysicalWidth), byref(o_nPhysicalHeight),
                          i_pBuffer, byref(io_nBufferSize))

        i_pBuffer = (c_byte * io_nBufferSize.value)()
        hama.GetImageData(self.filename,
                          i_nPhysicalXPos, i_nPhysicalYPos, i_nPhysicalZPos,
                          i_fMag, byref(o_nPhysicalWidth), byref(o_nPhysicalHeight),
                          i_pBuffer, byref(io_nBufferSize))

        stride = self.find_stride(io_nBufferSize.value, frame_width, frame_height)
        return Image.frombuffer("RGB",(frame_width,frame_height),i_pBuffer,"raw","BGR",stride,-1)


    def GetImagePx2D(self,width, height,x_coord_px,y_coord_px, magnification):
        """Return the image given the positions and extent in pixels. Works with 2D images.

        Arguments:

            - width: frame_width in pixels.
            - height: frame height in pixels.
            - x_coord_px: frame x center in pixels.
            - y_coord_px: frame y center in pixels.
            - magnification: long with the objective magnification.

        Output:

            PIL Image object containing cropped region.
        """
        im_info = self.GetMapInfo()

        # x_orig = physical_x - (physical_width/2.0)
        x_orig = im_info[2] - (im_info[0]/2.0)
        y_orig = im_info[3] - (im_info[1]/2.0)
        
        x_coord = int(x_orig + (x_coord_px * self.CONV_FACT) / magnification)
        y_coord = int(y_orig + (y_coord_px * self.CONV_FACT) / magnification)
        return self.GetImageData(width, height, x_coord, y_coord, 0, magnification)


    def GetCoordinatesInNm(self, x, y, magnification):
        xa = asarray(x)
        ya = asarray(y)
        im_info = self.GetMapInfo()

        # x_orig = physical_x - (physical_width/2.0)
        x_orig = im_info[2] - (im_info[0]/2.0)
        y_orig = im_info[3] - (im_info[1]/2.0)

        x_coord = x_orig + (xa * self.CONV_FACT) / magnification
        y_coord = y_orig + (ya * self.CONV_FACT) / magnification
        
        return x_coord.astype(int), y_coord.astype(int)


    def GetCoordinatesInPixels(self, x, y, magnification):
        xa = asarray(x)
        ya = asarray(y)
        im_info = self.GetMapInfo()

        x_orig = im_info[2] - (im_info[0]/2.0)
        y_orig = im_info[3] - (im_info[1]/2.0)

        x_px = (magnification/self.CONV_FACT)*(xa-x_orig)
        y_px = (magnification/self.CONV_FACT)*(ya-y_orig)

        return x_px.astype(int), y_px.astype(int)


    def GetNmToPixelFactor(self, magnification):
        return self.CONV_FACT/magnification


    def GetImageMap(self, magnification):
        """ Return the complete image at `magnification`.
        """
        width = self.GetImageWidthPx(magnification)
        height = self.GetImageHeightPx(magnification)
        center = self.GetMapInfo()[2:]

        return self.GetImageData(width, height, center[0], center[1], 0, magnification)


    def GetAnnotationData(self, ann, magnification, border=0):
        """ Return the image data within annotation.
        """
        xnm = ann.point_list[:,0]
        ynm = ann.point_list[:,1]
        xpx, ypx = self.GetCoordinatesInPixels(xnm, ynm, magnification)
        w = max(xpx) - min(xpx) + border
        h = max(ypx) - min(ypx) + border
        return self.GetImageData(w, h, ann.center[0], ann.center[1], ann.z, magnification)
       

    def CleanUp(self):
        return hama.CleanUp()
